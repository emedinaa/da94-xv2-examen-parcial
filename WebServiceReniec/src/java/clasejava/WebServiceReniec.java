/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasejava;

import java.sql.ResultSet;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author emedinaa
 */
@WebService(serviceName = "WebServiceReniec")
public class WebServiceReniec {

       private MySqlProvider mySqlProvider= new MySqlProvider("dni", "uigv", "uigv");
       private MySqlProvider mySqlVentasProvider= new MySqlProvider("VENTAS", "uigv", "uigv");
       
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    @WebMethod(operationName = "ObtenerDatosPEDIDO")
    public String ObtenerDatosPEDIDO(@WebParam(name = "pedido") String pedido)throws Exception {
       //TODO write your implementation code here:
      ResultSet rs = null;
      String datos= "";
      /*
      SELECT pedidos.NUM_PEDIDO,pedidos.FECHA_PEDIDO,pedidos.PRODUCTO,productos.DESCRIPCION,pedidos.CANT,pedidos.IMPORTE from pedidos, productos WHERE NUM_PEDIDO='112963' and pedidos.PRODUCTO= productos.ID_PRODUCTO 
      */
       rs = mySqlVentasProvider.Listar("select pedidos.NUM_PEDIDO,pedidos.FECHA_PEDIDO,pedidos.PRODUCTO,productos.DESCRIPCION,pedidos.CANT,pedidos.IMPORTE from pedidos, productos "+
               "where pedidos.NUM_PEDIDO=" + pedido + "" +
               " and pedidos.PRODUCTO= productos.ID_PRODUCTO;");
 
       while (rs.next()){
           datos  = datos + " " + rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3)+ " - " + rs.getString(4) + " - " + rs.getString(5) + " - " + rs.getString(6);
         }
       if (datos.compareTo("")==0){datos="No encontrado";};
 
       return datos ;
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "ObtenerDatosDNI")
    public String ObtenerDatosDNI(@WebParam(name = "dni") String dni)throws Exception {
       //TODO write your implementation code here:
        ResultSet rs = null;
      String datosdni= "";
       //conectamysql objc = new conectamysql();
       rs = mySqlProvider.Listar("select dni.dni,dni.nombre,dni.apepat,dni.apemat,dni.direccion," +
               "departamento.nombre,provincia.nombre,distrito.nombre from dni," +
               "departamento,provincia,distrito where dni.dni=" + dni + "" +
               " AND dni.Departamento_idDepartamento=departamento.idDepartamento " +
               "AND dni.provincia_idprovincia=provincia.idprovincia AND " +
               "dni.distrito_iddistrito=distrito.iddistrito;");
 
       while (rs.next()){
           datosdni  = datosdni + " " + rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3)+ " - " + rs.getString(4) + " - " + rs.getString(5) + " - " + rs.getString(6) + " - " + rs.getString(7) + " - " + rs.getString(8);
         }
       if (datosdni.compareTo("")==0){datosdni="No encontrado";};
 
       return datosdni ;
    }
}

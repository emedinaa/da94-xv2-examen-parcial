/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function nLog(message){
	if ( console && console.log ) {console.log(message);}
}

/*
    dataType: "JSON",
    encode : true,
 */
function sendData(data){
	$("#divResult").html("<h4>procesando... </h4>");
	$.ajax({
            url: "consumidorajax",
	    data: {
                textopedido : data
            },
	    type: "POST",
	    success: function(response){
	    	nLog("response "+response);
                var output = response;

	    	$("#divResult").html(output);
	    },
	    error:function(result,status, error){
	    	nLog("Error-------------");
	    	nLog("result "+result);
	    	nLog("status "+status);
	    	nLog("error "+error);
	    }
	});
}

function sendDataLibreria(data){
	$("#divResult").html("<h4>procesando... </h4>");
	$.ajax({
            url: "consumidorlibreriaajax",
	    data: {
                textoconsulta : data
            },
	    type: "POST",
	    success: function(response){
	    	nLog("response "+response);
                var output = response;

	    	$("#divResult").html(output);
	    },
	    error:function(result,status, error){
	    	nLog("Error-------------");
	    	nLog("result "+result);
	    	nLog("status "+status);
	    	nLog("error "+error);
	    }
	});
}

$(document).ready(function(){
    
    $("#btnBuscar").click(function(e) {
		e.preventDefault();
         var pedido = $('#textopedido').val();
         sendData(pedido);
    });
    
    $("#btnBuscarTexto").click(function(e) {
		e.preventDefault();
         var title = $('#textoconsulta').val();
         sendDataLibreria(title);
    });
    
});
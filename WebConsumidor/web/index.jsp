<%-- 
    Document   : index
    Created on : Sep 23, 2019, 3:29:57 PM
    Author     : emedinaa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Examen Parcial</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/examen.css"
    </head>
    <body>
        <h1 class="display-4">Examen parcial</h1>
    <form>
       <div class="form-group">
                <label for="textodni">Ingrese Pedido</label>
                <input type="text" class="form-control" id="textopedido" aria-describedby="dniHelp" placeholder="Pedido">
                <small id="dniHelp" class="form-text text-muted">Ingresar un número de pedido.</small>  
       </div>
       <button class="btn btn-primary" id="btnBuscar">Buscar</button>
    </form>
        
    <form>
       <div class="form-group">
                <label for="textoconsulta">Ingrese Nombre</label>
                <input type="text" class="form-control" id="textoconsulta" aria-describedby="textoHelp" placeholder="Nombre">
                <small id="textoHelp" class="form-text text-muted">Ingresar nombre de departamento.</small>   
       </div>
       <button class="btn btn-primary" id="btnBuscarTexto">Buscar</button>
    </form>
        
    <div class="container">
        <div class="alert alert-primary" role="alert" id="divResult">
            <p></p>
        </div>
    </div>

    </body> 
    <script language="JavaScript" type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="js/examen.js"></script>

</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import clasejava.Exception_Exception;
import clasejava.WebServiceReniec_Service;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author emedinaa
 */
@WebServlet(name = "consumidor", urlPatterns = {"/consumidor"})
public class consumidor extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_9090/WebServiceReniec/WebServiceReniec.wsdl")
    private WebServiceReniec_Service service_1;


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet consumidor</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet consumidor at " + request.getContextPath() + "</h1>");
               try{
                String textoconsulta = request.getParameter("textoconsulta");
                String textodni = request.getParameter("textodni");
                
                out.println("DNI "+textodni+" info: "+textodni+"</br></br>");
                out.println("consulta "+textoconsulta+" info: "+textoconsulta+"</br></br>");
       
                //misclasesjava.WebServiceMYSQL port = service.getWebServiceMYSQLPort();
                /*java.lang.String OBTENERINFO = textoconsulta;
                java.lang.String result = obtenerDatosDNI(OBTENERINFO);
                out.println("DNI "+textoconsulta+" info: "+result+"</br></br>");*/
                
                if(textodni!=null){
                    java.lang.String dni = textodni;
                    java.lang.String result = obtenerDatosDNI(dni);
                    out.println("DNI "+dni+" info: "+result+"</br></br>");
                }else if(textoconsulta!=null){
                    /*java.lang.String title = textoconsulta;
                    java.lang.String result =obtenerTitulo(title);
                    out.println("title "+title+" info: "+result+"</br></br>");*/
                }
                
            }catch(Exception e){
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String obtenerDatosDNI(java.lang.String dni) throws Exception_Exception {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        clasejava.WebServiceReniec port = service_1.getWebServiceReniecPort();
        return port.obtenerDatosDNI(dni);
    }


}

-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 24, 2019 at 07:21 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `VENTAS`
--

-- --------------------------------------------------------

--
-- Table structure for table `CLIENTES`
--

CREATE TABLE `CLIENTES` (
  `NUM_CLIE` decimal(4,0) NOT NULL,
  `EMPRESA` varchar(20) NOT NULL,
  `REP_CLIE` decimal(3,0) DEFAULT NULL,
  `LIM_CREDITO` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `CLIENTES`
--

INSERT INTO `CLIENTES` (`NUM_CLIE`, `EMPRESA`, `REP_CLIE`, `LIM_CREDITO`) VALUES
('2101', 'Jones Mfg.', '106', '65000.00'),
('2102', 'First Corp.', '101', '65000.00'),
('2103', 'Acme Mfg.', '105', '50000.00'),
('2105', 'AAA Investments', '101', '45000.00'),
('2106', 'Fred Lewis Corp', '102', '65000.00'),
('2107', 'Ace International', '110', '35000.00'),
('2108', 'Holm-Landis', '109', '55000.00'),
('2109', 'Chen Associates', '107', '25000.00'),
('2111', 'JCP Inc.', '103', '50000.00'),
('2112', 'Zetacorp', '108', '50000.00'),
('2113', 'Ian-Schmidt', '104', '20000.00'),
('2114', 'Orion Corp.', '102', '20000.00'),
('2115', 'Smithson Corp.', '101', '20000.00'),
('2117', 'J.P. Sinclair', '106', '35000.00'),
('2118', 'Midwest Systems', '108', '60000.00'),
('2119', 'Solomon Inc', '109', '25000.00'),
('2120', 'Rico Enterprises', '102', '50000.00'),
('2121', 'QMA Assoc.', '103', '45000.00'),
('2122', 'Three-Way Lines', '105', '30000.00'),
('2123', 'Carter-Sons.', '102', '40000.00'),
('2124', 'Peter Brothers', '107', '40000.00');

-- --------------------------------------------------------

--
-- Table structure for table `OFICINAS`
--

CREATE TABLE `OFICINAS` (
  `OFICINA` decimal(2,0) NOT NULL,
  `CIUDAD` varchar(20) NOT NULL,
  `REGION` varchar(7) NOT NULL,
  `DIR` decimal(3,0) DEFAULT NULL,
  `OBJETIVO` decimal(9,2) DEFAULT NULL,
  `VENTAS` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `OFICINAS`
--

INSERT INTO `OFICINAS` (`OFICINA`, `CIUDAD`, `REGION`, `DIR`, `OBJETIVO`, `VENTAS`) VALUES
('11', 'NEW YORK', 'ESTE', '106', '575000.00', '692637.00'),
('12', 'CHICAGO', 'ESTE', '104', '800000.00', '735042.00'),
('13', 'ATLANTA', 'ESTE', '105', '350000.00', '367911.00'),
('21', 'LOS ANGELES', 'OESTE', '108', '725000.00', '835915.00'),
('22', 'DENVER', 'OESTE', '108', '300000.00', '186042.00');

-- --------------------------------------------------------

--
-- Table structure for table `PEDIDOS`
--

CREATE TABLE `PEDIDOS` (
  `NUM_PEDIDO` decimal(6,0) NOT NULL,
  `FECHA_PEDIDO` date NOT NULL,
  `CLIE` decimal(4,0) NOT NULL,
  `REP` decimal(3,0) DEFAULT NULL,
  `FAB` varchar(3) NOT NULL,
  `PRODUCTO` varchar(6) NOT NULL,
  `CANT` decimal(3,0) NOT NULL,
  `IMPORTE` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PEDIDOS`
--

INSERT INTO `PEDIDOS` (`NUM_PEDIDO`, `FECHA_PEDIDO`, `CLIE`, `REP`, `FAB`, `PRODUCTO`, `CANT`, `IMPORTE`) VALUES
('112961', '2014-12-17', '2117', '106', 'REI', '2A44L', '7', '31500.00'),
('112963', '2014-12-17', '2103', '105', 'ACI', '41004', '28', '3276.00'),
('112968', '2014-10-12', '2102', '101', 'ACI', '41004', '34', '3978.00'),
('112975', '2014-10-12', '2111', '103', 'REI', '2A44G', '6', '2100.00'),
('112979', '2015-10-12', '2114', '102', 'ACI', '41002', '6', '15000.00'),
('112983', '2014-12-27', '2103', '105', 'ACI', '41004', '6', '702.00'),
('112987', '2014-12-31', '2103', '105', 'ACI', '4100Y', '11', '27500.00'),
('112989', '2015-01-03', '2101', '106', 'FEA', '114', '6', '1458.00'),
('112992', '2014-11-04', '2118', '108', 'ACI', '41002', '10', '760.00'),
('112993', '2014-01-04', '2106', '102', 'REI', '2A45C', '24', '1896.00'),
('112997', '2015-01-08', '2124', '107', 'BIC', '41003', '1', '652.00'),
('113003', '2015-01-25', '2108', '109', 'IMM', '779C', '3', '5625.00'),
('113007', '2015-01-08', '2112', '108', 'IMM', '773C', '3', '2925.00'),
('113012', '2015-01-11', '2111', '105', 'ACI', '41003', '35', '3745.00'),
('113013', '2015-01-14', '2118', '108', 'BIC', '41003', '1', '652.00'),
('113024', '2015-01-20', '2114', '108', 'QSA', 'XK47', '20', '7100.00'),
('113027', '2015-01-22', '2103', '105', 'ACI', '41002', '54', '4104.00'),
('113034', '2015-01-29', '2107', '110', 'REI', '2A45C', '8', '632.00'),
('113036', '2015-01-30', '2107', '110', 'ACI', '41002', '9', '22500.00'),
('113042', '2015-02-02', '2113', '101', 'REI', '2A44R', '5', '22500.00'),
('113045', '2014-02-02', '2112', '108', 'REI', '2A44R', '10', '45000.00'),
('113048', '2015-02-10', '2120', '102', 'IMM', '779C', '2', '3750.00'),
('113049', '2015-02-10', '2118', '108', 'QSA', 'XK47', '2', '776.00'),
('113051', '2015-02-10', '2118', '108', 'QSA', 'XK47', '4', '1420.00'),
('113055', '2015-02-15', '2108', '101', 'ACI', '4100X', '6', '150.00'),
('113057', '2014-02-18', '2111', '103', 'ACI', '4100X', '24', '600.00'),
('113058', '2015-02-23', '2108', '109', 'FEA', '112', '10', '1480.00'),
('113062', '2015-02-24', '2124', '107', 'FEA', '114', '10', '2430.00'),
('113065', '2015-02-27', '2106', '102', 'QSA', 'XK47', '6', '2130.00'),
('113069', '2015-03-02', '2109', '107', 'IMM', '775C', '22', '31350.00');

-- --------------------------------------------------------

--
-- Table structure for table `PRODUCTOS`
--

CREATE TABLE `PRODUCTOS` (
  `ID_FAB` varchar(3) NOT NULL,
  `ID_PRODUCTO` varchar(5) NOT NULL,
  `DESCRIPCION` varchar(20) NOT NULL,
  `PRECIO` decimal(6,2) NOT NULL,
  `EXISTENCIA` decimal(4,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PRODUCTOS`
--

INSERT INTO `PRODUCTOS` (`ID_FAB`, `ID_PRODUCTO`, `DESCRIPCION`, `PRECIO`, `EXISTENCIA`) VALUES
('ACI', '41001', 'ARTICULO TIPO 1', '55.00', '277'),
('ACI', '41002', 'ARTICULO TIPO 2', '76.00', '167'),
('ACI', '41003', 'ARTICULO TIPO 3', '107.00', '207'),
('ACI', '41004', 'ARTICULO TIPO 4', '117.00', '139'),
('ACI', '4100X', 'AJUSTADOR', '25.00', '37'),
('ACI', '4100Y', 'EXTRACTOR', '2750.00', '25'),
('ACI', '4100Z', 'MONTADOR', '2500.00', '28'),
('BIC', '41003', 'MANIVELA', '652.00', '3'),
('BIC', '41089', 'RETN', '225.00', '78'),
('BIC', '41672', 'PLATE', '180.00', '0'),
('FEA', '112', 'CUBIERTA', '148.00', '115'),
('FEA', '114', 'BANCADA MOTOR', '243.00', '15'),
('IMM', '773C', 'RIOSTRA 1-2TM', '975.00', '28'),
('IMM', '775C', 'RIOSTRA 1TM', '1425.00', '5'),
('IMM', '779C', 'RIOSTRA 2TM', '1875.00', '9'),
('IMM', '887H', 'SOPORTE RIOSTRA', '54.00', '223'),
('IMM', '887P', 'PERNO RIOSTRA', '250.00', '24'),
('IMM', '887X', 'RETENEDOR RIOSTRA', '475.00', '32'),
('QSA', 'XK47', 'REDUCTOR', '355.00', '38'),
('QSA', 'XK48', 'REDUCTOR', '134.00', '203'),
('QSA', 'XK48A', 'REDUCTOR', '117.00', '37'),
('REI', '2A44G', 'PASADOR BISAGRA', '350.00', '14'),
('REI', '2A44L', 'BISAGRA IZQDA', '4500.00', '12'),
('REI', '2A44R', 'BISAGRA DCHA', '4500.00', '12'),
('REI', '2A45C', 'V STAGO TRINQUETE', '79.00', '210');

-- --------------------------------------------------------

--
-- Table structure for table `REPVENTAS`
--

CREATE TABLE `REPVENTAS` (
  `NUM_EMPL` decimal(3,0) NOT NULL,
  `NOMBRE` varchar(25) DEFAULT NULL,
  `EDAD` decimal(2,0) DEFAULT NULL,
  `OFICINA_REP` decimal(2,0) DEFAULT NULL,
  `TITULO` varchar(10) DEFAULT NULL,
  `CONTRATO` date DEFAULT NULL,
  `DIRECTOR` decimal(3,0) DEFAULT NULL,
  `CUOTA` decimal(9,2) DEFAULT NULL,
  `VENTAS` decimal(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `REPVENTAS`
--

INSERT INTO `REPVENTAS` (`NUM_EMPL`, `NOMBRE`, `EDAD`, `OFICINA_REP`, `TITULO`, `CONTRATO`, `DIRECTOR`, `CUOTA`, `VENTAS`) VALUES
('101', 'DAN ROBERTS', '45', '12', 'REP VENTAS', '2006-10-20', '104', '300000.00', '305673.00'),
('102', 'SUE SMITH', '48', '21', 'REP VENTAS', '2006-12-10', '108', '350000.00', '474050.00'),
('103', 'PAUL CRUZ', '29', '12', 'REP VENTAS', '2007-03-01', '104', '275000.00', '286775.00'),
('104', 'BOB SMITH', '33', '12', 'DIR VENTAS', '2007-05-19', '106', '200000.00', '142594.00'),
('105', 'BILL ADAMS', '37', '13', 'REP VENTAS', '2008-02-12', '104', '350000.00', '367911.00'),
('106', 'SAM CLARK', '52', '11', 'VP VENTAS', '2008-06-14', NULL, '275000.00', '299912.00'),
('107', 'NANCY ANGELLY', '49', '22', 'REP VENTAS', '2008-11-14', '108', '300000.00', '186042.00'),
('108', 'LARRY FITCH', '62', '21', 'DIR VENTAS', '2009-10-12', '106', '350000.00', '361865.00'),
('109', 'MARY JONES', '31', '11', 'REP VENTAS', '2009-10-12', '106', '300000.00', '392725.00'),
('110', 'TOM SNYDER', '41', NULL, 'REP VENTAS', '2010-01-13', '101', NULL, '75985.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CLIENTES`
--
ALTER TABLE `CLIENTES`
  ADD PRIMARY KEY (`NUM_CLIE`);

--
-- Indexes for table `OFICINAS`
--
ALTER TABLE `OFICINAS`
  ADD PRIMARY KEY (`OFICINA`);

--
-- Indexes for table `PEDIDOS`
--
ALTER TABLE `PEDIDOS`
  ADD PRIMARY KEY (`NUM_PEDIDO`);

--
-- Indexes for table `PRODUCTOS`
--
ALTER TABLE `PRODUCTOS`
  ADD PRIMARY KEY (`ID_FAB`,`ID_PRODUCTO`);

--
-- Indexes for table `REPVENTAS`
--
ALTER TABLE `REPVENTAS`
  ADD PRIMARY KEY (`NUM_EMPL`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasejava;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;

/**
 *
 * @author emedinaa
 */
public class conectarmysql {
        
   public Connection getConextion() {
       try {
           Class.forName("com.mysql.jdbc.Driver");
           String url ="jdbc:mysql://localhost:3306/libreria";
           return (DriverManager.getConnection(url,"uigv","uigv"));
       } catch (Exception e) {
           System.out.println("error"+e.getMessage());
       }
       return null;
       }
   
   public ResultSet Listar(String sql) {
       Statement st = null;
       ResultSet rs = null;
       try {
           Connection conn = this.getConextion();
           st = conn.createStatement();
           rs = st.executeQuery(sql);
       } catch (Exception e) {
           System.out.println("Error:"+ e.getMessage());
       }
       return rs;
   }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasejava;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.ResultSet;

/**
 *
 * @author emedinaa
 */
@WebService(serviceName = "WebServiceLibro")
public class WebServiceLibro {

    private MySqlProvider mySqlProvider= new MySqlProvider("libreria", "uigv", "uigv");
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "ObtenerTitulo")
    public String ObtenerTitulo(@WebParam(name = "Titulo") String Titulo) throws Exception {
               //TODO write your implementation code here:
      ResultSet rs = null;
      String datos= "";
       //conectarmysql objc = new conectarmysql();
       
       rs = mySqlProvider.Listar("select libro.ISBN,autor.Nombre,libro.Stock,libro.Precio," +
"Categoria.Categoria,libro.Nombre,Idioma.Idioma from autor,libro,Categoria," +
               "Idioma where libro.Nombre like '%" + Titulo + "%'" +
               " and autor.idAutor=libro.Autor_idAutor and" +
               " Categoria.idCategoria=libro.Categoria_idCategoria and" +
               " Idioma.idIdioma=libro.Idioma_idIdioma;");
        while (rs.next()){
           datos  = datos + " " + rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3)+ " - " + rs.getString(4) + " - " + rs.getString(5) + " - " + rs.getString(6) + " - " + rs.getString(7);
        }
       if (datos.compareTo("")==0){datos="No encontrado";};
       return datos ;
        }

}


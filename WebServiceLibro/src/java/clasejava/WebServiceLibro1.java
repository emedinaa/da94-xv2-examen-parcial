/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasejava;

import java.sql.ResultSet;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author emedinaa
 */
@WebService(serviceName = "WebServiceLibro1")
public class WebServiceLibro1 {

    private MySqlProvider mySqlProvider= new MySqlProvider("libreria", "uigv", "uigv");
    private MySqlProvider mySqlRHProvider= new MySqlProvider("RH", "uigv", "uigv");
        
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
    
    @WebMethod(operationName = "ObtenerDepartment")
    public String ObtenerDepartment(@WebParam(name = "Name") String Name) throws Exception{
          ResultSet rs = null;
          String datos= "";
          
           rs = mySqlRHProvider.Listar("select departments.department_id,departments.department_name, employees.first_name, employees.last_name,locations.street_address, locations.city FROM departments,employees,locations where departments.department_name like '%" + Name + "%'" +
               " and departments.manager_id= employees.employee_id and departments.location_id=locations.location_id;");
           while (rs.next()){
            datos  = datos + " " + rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3)+ " - " + rs.getString(4) + " - " + rs.getString(5) + " - " + rs.getString(6);
           }
            
           if (datos.compareTo("")==0){
               datos="No encontrado";
           };
           return datos ;
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "ObtenerTitulo")
    public String ObtenerTitulo(@WebParam(name = "Titulo") String Titulo) throws Exception {
               //TODO write your implementation code here:
      ResultSet rs = null;
      String datos= "";
       //conectarmysql objc = new conectarmysql();
       
       rs = mySqlProvider.Listar("select libro.ISBN,autor.Nombre,libro.Stock,libro.Precio," +
"Categoria.Categoria,libro.Nombre,Idioma.Idioma from autor,libro,Categoria," +
               "Idioma where libro.Nombre like '%" + Titulo + "%'" +
               " and autor.idAutor=libro.Autor_idAutor and" +
               " Categoria.idCategoria=libro.Categoria_idCategoria and" +
               " Idioma.idIdioma=libro.Idioma_idIdioma;");
        while (rs.next()){
           datos  = datos + " " + rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3)+ " - " + rs.getString(4) + " - " + rs.getString(5) + " - " + rs.getString(6) + " - " + rs.getString(7);
        }
       if (datos.compareTo("")==0){datos="No encontrado";};
       return datos ;
        }

}
